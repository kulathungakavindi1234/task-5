import java.util.List;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {
		List<Bowler> bowlers = DataReader.readBowlersFromFile("C:/Users/HP/Desktop/Java/Task_5/src/Bowling_ODI.csv");

		long startTime = System.currentTimeMillis();
		Bowler bestBowlerSequential = bowlers.stream().max(Bowler::compareTo).orElse(null);
		long endTime = System.currentTimeMillis();
		long sequentialTime = endTime - startTime;

		// Compare bowlers using parallel stream
		startTime = System.currentTimeMillis();
		Bowler bestBowlerParallel = bowlers.parallelStream().max(Bowler::compareTo).orElse(null);
		endTime = System.currentTimeMillis();
		long parallelTime = endTime - startTime;

		// Print the best bowlers and execution times
		System.out.println("Best Bowler (Sequential): " + bestBowlerSequential);
		System.out.println("Best Bowler (Parallel): " + bestBowlerParallel);
		System.out.println("Sequential Time: " + sequentialTime + " ms");
		System.out.println("Parallel Time: " + parallelTime + " ms");

		startTime = System.currentTimeMillis();
		List<Bowler> filteredBowlersSequential = bowlers.stream()
				.filter(bowler -> bowler.getFourWicketHauls() > 5 && bowler.getFiveWicketHauls() > 4)
				.collect(Collectors.toList());
		endTime = System.currentTimeMillis();
		sequentialTime = endTime - startTime;

		// Filter bowlers with more than 5 four-wicket hauls and more than 4 five-wicket
		// hauls using parallel stream
		startTime = System.currentTimeMillis();
		List<Bowler> filteredBowlersParallel = bowlers.parallelStream()
				.filter(bowler -> bowler.getFourWicketHauls() > 5 && bowler.getFiveWicketHauls() > 4)
				.collect(Collectors.toList());
		endTime = System.currentTimeMillis();
		parallelTime = endTime - startTime;

		// Print the execution times
		System.out.println("Sequential Time: " + sequentialTime + " ms");
		System.out.println("Parallel Time: " + parallelTime + " ms");
	}

}
