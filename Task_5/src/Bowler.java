public class Bowler implements Comparable<Bowler> {
    private String playerName;
    private String span;
    private int matches;
    private int innings;
    private int balls;
    private int runs;
    private int wickets;
    private String bbi;
    private double average;
    private double economy;
    private double strikeRate;
    private int fourWicketHauls;
    private int fiveWicketHauls;

    public Bowler(String playerName, String span, int matches, int innings, int balls, int runs,
                         int wickets, String bbi, double average, double economy, double strikeRate,
                         int fourWicketHauls, int fiveWicketHauls) {
        this.playerName = playerName;
        this.span = span;
        this.matches = matches;
        this.innings = innings;
        this.balls = balls;
        this.runs = runs;
        this.wickets = wickets;
        this.bbi = bbi;
        this.average = average;
        this.economy = economy;
        this.strikeRate = strikeRate;
        this.fourWicketHauls = fourWicketHauls;
        this.fiveWicketHauls = fiveWicketHauls;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getSpan() {
        return span;
    }

    public void setSpan(String span) {
        this.span = span;
    }

    public int getMatches() {
        return matches;
    }

    public void setMatches(int matches) {
        this.matches = matches;
    }

    public int getInnings() {
        return innings;
    }

    public void setInnings(int innings) {
        this.innings = innings;
    }

    public int getBalls() {
        return balls;
    }

    public void setBalls(int balls) {
        this.balls = balls;
    }

    public int getRuns() {
        return runs;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public int getWickets() {
        return wickets;
    }

    public void setWickets(int wickets) {
        this.wickets = wickets;
    }

    public String getBbi() {
        return bbi;
    }

    public void setBbi(String bbi) {
        this.bbi = bbi;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public double getEconomy() {
        return economy;
    }

    public void setEconomy(double economy) {
        this.economy = economy;
    }

    public double getStrikeRate() {
        return strikeRate;
    }

    public void setStrikeRate(double strikeRate) {
        this.strikeRate = strikeRate;
    }

    public int getFourWicketHauls() {
        return fourWicketHauls;
    }

    public void setFourWicketHauls(int fourWicketHauls) {
        this.fourWicketHauls = fourWicketHauls;
    }

    public int getFiveWicketHauls() {
        return fiveWicketHauls;
    }

    public void setFiveWicketHauls(int fiveWicketHauls) {
        this.fiveWicketHauls = fiveWicketHauls;
    }

    @Override
    public int compareTo(Bowler other) {
        return Integer.compare(this.wickets, other.wickets);
    }
    
    @Override
    public String toString() {
        return "Player Name: " + playerName +
                "\nMatches: " + matches +
                "\nInnings: " + innings +
                "\nWickets: " + wickets +
                "\nRuns: " + runs +
                "\nEconomy: " + economy;
    }
}
