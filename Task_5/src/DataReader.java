import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DataReader {

    private static final String CSV_SEPARATOR = ",";

    public static List<Bowler> readBowlersFromFile(String filePath) {
        List<Bowler> bowlers = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            bowlers = reader.lines()
                    .skip(1) // Skip the first row
                    .map(line -> line.split(CSV_SEPARATOR))
                    .map(DataReader::createBowlerFromData)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bowlers;
    }

    private static Bowler createBowlerFromData(String[] data) {
        String playerName = data[1];
        String span = data[2];
        int matches = parseIntegerOrDefault(data[3], 0);
        int innings = parseIntegerOrDefault(data[4], 0);
        int balls = parseIntegerOrDefault(data[5], 0);
        int runs = parseIntegerOrDefault(data[6], 0);
        int wickets = parseIntegerOrDefault(data[7], 0);
        String bbi = data[8];
        double average = parseDoubleOrDefault(data[9], 0);
        double economy = parseDoubleOrDefault(data[10], 0);
        double strikeRate = parseDoubleOrDefault(data[11], 0);
        int fourWicketHauls = parseIntegerOrDefault(data[12], 0);
        int fiveWicketHauls = parseIntegerOrDefault(data[13], 0);
        
        

        return new Bowler(playerName, span, matches, innings, balls, runs, wickets, bbi, average, economy,
                strikeRate, fourWicketHauls, fiveWicketHauls);
    }

    private static int parseIntegerOrDefault(String value, int defaultValue) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    private static double parseDoubleOrDefault(String value, double defaultValue) {
        if (value.equals("-")) {
            return defaultValue;
        }
        return Double.parseDouble(value);
    }
}
